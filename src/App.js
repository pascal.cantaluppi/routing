import React from "react";
import "./App.css";

import Nav from "./components/Nav";
import Characters from "./components/Characters";
import Character from "./components/Character";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/characters" exact component={Characters} />
          <Route path="/characters/:id" component={Character} />
          <Route component={NoMatch} />
        </Switch>
      </div>
    </Router>
  );
}

const Home = () => (
  <div>
    <h1>React Router Example</h1>
    <p>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua.
    </p>
    <p>
      <img src="./img/Mr.Meeseeks.png" alt="Mr. Meeseeks"></img>
    </p>
  </div>
);

const NoMatch = ({ location }) => (
  <div>
    <h1>404 - Page not found</h1>
    <p>
      No match for Route <code>{location.pathname}</code>
    </p>
    <p>
      <img src="../img/portal.png" alt="Portal"></img>
    </p>
  </div>
);

export default App;
