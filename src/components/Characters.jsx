import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Characters() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetchItems();
  }, []);

  const fetchItems = async () => {
    // https://github.com/public-apis/public-apis
    const data = await fetch("https://rickandmortyapi.com/api/character");
    const items = await data.json();
    //console.log(items);
    setItems(items.results);
  };

  return (
    <div className="App">
      <h1>Characters</h1>
      {items.map(item => (
        <p key={item.id}>
          <Link to={`/characters/${item.id}`}>{item.name}</Link>
        </p>
      ))}
    </div>
  );
}

export default Characters;
