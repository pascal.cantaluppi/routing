import React, { useEffect, useState } from "react";

function Character({ match }) {
  const [item, setItem] = useState({});

  useEffect(() => {
    fetchItem();
  });

  const fetchItem = async () => {
    const data = await fetch(
      `https://rickandmortyapi.com/api/character/${match.params.id}`
    );
    const item = await data.json();
    //console.log(item);
    setItem(item);
  };

  return (
    <div>
      <h3>Character</h3>
      <p>{item.name}</p>
      <p>
        <img src={item.image} alt={item.name} border="1" />
      </p>
    </div>
  );
}

export default Character;
