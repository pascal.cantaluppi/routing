import React from "react";
import { Link } from "react-router-dom";

function Nav() {
  const navStyle = {
    color: "white"
  };

  return (
    <div>
      <nav>
        <h3>Router App</h3>
        <ul className="nav-links">
          <Link style={navStyle} to="/">
            <li>Home</li>
          </Link>
          <Link style={navStyle} to="/characters">
            <li>Characters</li>
          </Link>
          <Link style={navStyle} to="/deadlink">
            <li>Deadlink</li>
          </Link>
        </ul>
      </nav>
    </div>
  );
}

export default Nav;
