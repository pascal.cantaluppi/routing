# React Router

<p>
  <a href="https://reactjs.org/docs/context.html#when-to-use-context" target="blank"><img src="https://gitlab.com/pascal.cantaluppi/routing/raw/master/public/img/react-router.png" width="180" alt="React Router" />
</p>

## Routing Examples

<p>React Router is a collection of navigational components that compose declaratively with your application.</p>

- <a href="https://reacttraining.com/react-router/" target="blank">https://reacttraining.com/react-router/</a>
